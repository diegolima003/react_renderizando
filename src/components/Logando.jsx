import React, {useState} from 'react'

export default () => {

    const [login, setLogin] =useState(false)

    return (
        <>
            <h1>Exemplo 2 de renderização</h1>
            <h3>{login?'Usuário Logado':'Usuário Não Logado'}</h3>
            <button onClick={()=>setLogin(!login)}>Logar/deslogar</button>
        </>
        
    )
}