import React, { useState } from 'react'

export default () => {

    const [cor, setCor] = useState(0)

    // definindo as cores
    const preto = { color: '#000' }
    const azul = { color: '#00f' }
    const verde = { color: '#0f0' }
    const vermelho = { color: '#f00' }

    const showCor = (c) => {
        if (c == 1) {
            return azul
        } else if (c == 2) {
            return verde
        } else if (c == 3) {
            return vermelho
        } else {
            return preto
        }
    }


    const mudarCor = () => {
        setCor(cor+1)   // pegar a cor padrão(preto) mais 1
        if (cor > 2) {   // ser a cor padrão for maior que 2
            setCor(0)    // volta sempre para o 0(cor padrão) isso faz ficar o rodízio de cor
        }
    }

    
    return (
        <div style={{ textAlign: 'center' }}>
            <h1 style={showCor(cor)}>Esse texto mudara de cor</h1>
            <button onClick={() => mudarCor()}>Mudar cor</button>
        </div>

    )
}