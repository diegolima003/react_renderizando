import React from 'react'
import Cores from './components/Cores'
import Logando from './components/Logando'


//Renderização é basicamente mostra ou não mostra algo.

export default ()=> {

  const saldar=()=> {
    const hora = new Date().getHours()
       
    if(hora >= 0 && hora < 12){
      return <p>Bom Dia!</p>
    }else if(hora >= 12 && hora < 18){
      return <p>Boa Tarde!!</p>
    }else{
      return <p>Boa Noite!!!</p>
    }
    
  }
    return(
      <>
        <h1>Renderizando a hora</h1>
        <h3>{saldar()}</h3>
        < Logando />
        < Cores />
      </>
    )
    
}